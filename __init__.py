from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import json

with open("config.json") as config_f:
    config = json.load(config_f)

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = f"mysql://{config['db_user']}:{config['db_password']}@{config['db_host']}:{config['db_port']}/{config['db_name']}"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = config.get("secret_key") or "test"

db = SQLAlchemy(app)

from booklist import models
from booklist import routes

if __name__=="__main__":
    app.run()
