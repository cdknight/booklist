from booklist import app, db
from flask import render_template, jsonify, request, url_for, flash, redirect
from booklist.models import *
import requests
import json

@app.route('/')
def index():
    return redirect(url_for('displaylist'))

@app.route("/list")
def displaylist():
    suggestions = Suggestion.query.all()
    return render_template("booklist.html", suggestions=suggestions)

@app.route("/suggestion/add", methods=["GET", "POST"])
def add_book():
    if request.method == "POST":
        suggestee_ids = request.values.get("suggestees")
        label_ids = request.values.get("labels")
        isbn = request.values.get("isbn")

        # Get ISBN data
        #        bookdata = requests.get(f"https://openlibrary.org/api/books?bibkeys=ISBN:{isbn}&format=json&jscmd=data")
        #        bookdata_dict = json.loads(bookdata.text)
        new_suggestion = Suggestion(isbn=isbn)
        print(new_suggestion.suggestees)
        if suggestee_ids:
            for id in suggestee_ids:
                new_suggestion.suggestees.append(Suggestee.query.filter_by(id=id).first())
        if label_ids:
            for id in label_ids:
                new_suggestion.labels.append(Label.query.filter_by(id=id).first())

        db.session.add(new_suggestion)
        db.session.commit()

        flash("Suggestion added succesfully.")
        return redirect(url_for('displaylist'))

    suggestees = Suggestee.query.all()
    labels = Label.query.all()

    return render_template("add_book.html", suggestees=suggestees, labels=labels, title="Add Suggestion")

@app.route("/suggestion/delete/<id>")
def delete_suggestion(id):
    suggestion = Suggestion.query.filter_by(id=id).first()
    if suggestion == None:
        flash("The suggestion to delete that you provided was not valid.")
        return redirect(url_for("displaylist"))

    db.session.delete(suggestion)
    db.session.commit()
    flash("The suggestion was deleted successfully.")
    return redirect(url_for("displaylist"))


@app.route("/suggestion/edit/<id>", methods=["GET", "POST"])
def edit_suggestion(id):
    if request.method == "POST":
        suggestee_ids = request.values.get("suggestees")
        label_ids = request.values.get("labels")
        isbn = request.values.get("isbn")

        if suggestee_ids:
            for id in suggestee_ids:
                new_suggestion.suggestees.append(Suggestee.query.filter_by(id=id).first())
        if label_ids:
            for id in label_ids:
                new_suggestion.labels.append(Label.query.filter_by(id=id).first())

        

    suggestees = Suggestee.query.all()
    labels = Label.query.all()

    suggestion = Suggestion.query.filter_by(id=id).first()
    return render_template("add_book.html", suggestees=suggestees, labels=labels, title="Edit Suggestion", suggestion=suggestion)

@app.route("/label/add", methods=["GET", "POST"])
def add_label():
    if request.method == "POST":
        new_label = Label(**request.values)
        db.session.add(new_label)
        db.session.commit()
    return render_template("add_label.html")


@app.route('/suggestee/add', methods=["GET", "POST"])
def add_suggestee():
    if request.method == "POST":
        new_suggestee = Suggestee(**request.values)
        db.session.add(new_suggestee)
        db.session.commit()

    return render_template("add_suggestee.html")
