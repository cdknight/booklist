from booklist import db
import requests
import json

suggestion_suggestees = db.Table("suggestion_suggestees", db.Model.metadata,
    db.Column("suggestion_id", db.Integer, db.ForeignKey("suggestions.id")),
    db.Column("suggestee_id", db.Integer, db.ForeignKey("suggestees.id"))
)

suggestion_labels = db.Table("suggestion_labels", db.Model.metadata,
    db.Column("suggestion_id", db.Integer, db.ForeignKey("suggestions.id")),
    db.Column("labels_id", db.Integer, db.ForeignKey("labels.id"))
)

class Suggestion(db.Model):
    __tablename__ = "suggestions"
    id = db.Column(db.Integer, primary_key=True)
    isbn = db.Column(db.String(13), index=True, unique=True, nullable=False)
    suggestees = db.relationship("Suggestee", secondary=suggestion_suggestees, backref="suggestions")
    labels = db.relationship("Label", secondary=suggestion_labels, backref="suggestions")

    @property
    def title(self):
        # Get ISBN data
        bookdata = requests.get(f"https://openlibrary.org/api/books?bibkeys=ISBN:{self.isbn}&format=json&jscmd=data")
        bookdata_dict = json.loads(bookdata.text)

        title = bookdata_dict[f"ISBN:{self.isbn}"]['title']
        if bookdata_dict[f"ISBN:{self.isbn}"].get("subtitle"):
            title += ": " +  bookdata_dict[f"ISBN:{self.isbn}"].get("subtitle")

        return title


class Label(db.Model):
    __tablename__ = "labels"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(25), index=True, unique=True, nullable=False)


class Suggestee(db.Model):
    __tablename__ = "suggestees"
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(25), index=True, unique=True, nullable=False)
    last_name = db.Column(db.String(25), index=True, unique=True, nullable=False)

    description = db.Column(db.String(500))

    @property
    def name(self):
        return self.first_name + " " + self.last_name

