# Book Suggestion Tracker

A small webapp to kepe track of your book suggestions.

### Install Dependencies

Run `pip3 install --user mysqlclient flask_sqlalchemy flask`

## Setup

To start, make sure you copy config.json.template to config.json and update the values to correspond with your MySQL/MariaDB server's information.

After doing that, run `export FLASK_APP=__init__.py` and access a shell to execute commands relating to this program with `flask shell`

```python
from booklist import db
db.create_all()
```
## Start

Start the application with `flask run`

